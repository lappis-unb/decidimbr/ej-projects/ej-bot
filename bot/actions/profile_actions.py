from typing import Any, Dict, List, Text

from actions.helpers.profile_actions_helpers import (
    NextProfileQuestionHelper,
    ValidateProfileQuestionHelper,
)
from actions.logger import custom_logger
from rasa_sdk import Action, Tracker
from rasa_sdk.events import EventType
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormValidationAction
from rasa_sdk.types import DomainDict


class ActionAskProfileQuestion(Action):
    """
    https://rasa.com/docs/rasa/forms/#using-a-custom-action-to-ask-for-the-next-slot
    """

    def name(self) -> Text:
        return "action_ask_profile_question"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        # If you want to add new verifications during this action call,
        # you need to implement a new Checker.
        action_checkers = self.get_helpers(
            tracker,
            dispatcher=dispatcher,
        )

        for checker in action_checkers:
            if checker.has_slots_to_return():
                self.slots = checker.slots
                break

        return self.slots

    def get_helpers(self, tracker, **kwargs) -> list:
        """
        Return a list of Checkers. They will be evaluated in sequence.
        """
        dispatcher = kwargs["dispatcher"]
        return [NextProfileQuestionHelper(tracker=tracker, dispatcher=dispatcher)]


class ValidateProfileForm(FormValidationAction):
    """
    https://rasa.com/docs/rasa/forms/#validating-form-input
    """

    def name(self) -> Text:
        return "validate_profile_form"

    def validate_profile_question(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        if not slot_value:
            return {}

        action_checkers = self.get_helpers(
            tracker, dispatcher=dispatcher, slot_value=slot_value
        )

        for checker in action_checkers:
            custom_logger(checker, _type="string")
            if checker.has_slots_to_return():
                self.slots = checker.slots

        return self.slots

    def get_helpers(self, tracker, **kwargs) -> list:
        """
        Return a list of Checkers. They will be evaluated in sequence.
        """

        dispatcher = kwargs["dispatcher"]
        slot_value = kwargs["slot_value"]
        return [
            ValidateProfileQuestionHelper(
                tracker=tracker, dispatcher=dispatcher, slot_value=slot_value
            )
        ]

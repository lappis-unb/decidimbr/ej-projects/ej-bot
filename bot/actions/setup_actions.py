from actions.helpers.setup_actions_helpers import (
    GetBoardSlotsHelper,
    GetConversationSlotsHelper,
)
from actions.helpers.api_error_checker import EJClientErrorManager
from actions.logger import custom_logger
from ej.vote import SlotsType
from ej.user import User
from rasa_sdk import Action


class ActionGetConversation(Action):
    """
    Authenticates the chatbot user on EJ API and requests initial conversation data.
    This action is called on the beginner of every new conversation.
    """

    def name(self):
        return "action_get_conversation"

    def run(self, dispatcher, tracker, domain):
        user = User(tracker)
        try:
            user.authenticate()
        except Exception as e:
            custom_logger(f"ERROR: {e}")
            ej_client_error_manager = EJClientErrorManager()
            return ej_client_error_manager.get_slots()

        self.slots = []

        checkers = self.get_helpers(tracker, user=user, dispatcher=dispatcher)
        for checker in checkers:
            if checker.has_slots_to_return():
                self.slots = checker.slots
                break

        return self.slots

    def get_helpers(self, tracker, **kwargs) -> list:
        dispatcher = kwargs["dispatcher"]
        user = kwargs["user"]
        return [
            GetConversationSlotsHelper(
                dispatcher=dispatcher, user=user, slots_type=SlotsType.LIST
            ),
            GetBoardSlotsHelper(
                dispatcher=dispatcher, user=user, slots_type=SlotsType.LIST
            ),
        ]

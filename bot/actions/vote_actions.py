from typing import Any, Dict, List, Text

from actions.helpers.api_error_checker import EJClientErrorManager
from actions.helpers.setup_actions_helpers import get_setup_slots
from actions.helpers.vote_actions_helpers import (
    ExternalAuthenticationSlotsHelper,
    NeedToAskAboutProfileHelper,
    NextCommentSlotsHelper,
    RemainingCommentsSlotsHelper,
    UserCanAddCommentsSlotsHelper,
)
from actions.logger import custom_logger
from ej.conversation import Conversation
from ej.settings import EJCommunicationError
from ej.user import User
from ej.vote import SlotsType, Vote, VoteDialogue
from rasa_sdk import Action, FormValidationAction, Tracker
from rasa_sdk.events import EventType
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict


class ActionSendConversationText(Action):
    def name(self) -> Text:
        return "action_start_participatory_process"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        user = User(tracker)
        conversation = Conversation(tracker)

        conversation.user_statistics = tracker.get_slot("user_statistics")
        if conversation.get_voted_comments():
            dispatcher.utter_message(response="utter_resume_conversation")
        else:
            dispatcher.utter_message(response="utter_bot_introduction")
            dispatcher.utter_message(response="utter_show_topic_of_discussion")
        return get_setup_slots(conversation, user)


class ActionAskVote(Action):
    """
    This action is called when the vote_form is active.
    It shows a comment for user to vote on, and also their statistics in the conversation.

    https://rasa.com/docs/rasa/forms/#using-a-custom-action-to-ask-for-the-next-slot
    """

    def name(self) -> Text:
        return "action_ask_vote"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        conversation = Conversation(tracker)
        user = User(tracker)
        conversation.user_statistics = tracker.get_slot("user_statistics")

        checkers = self.get_helpers(
            tracker, dispatcher=dispatcher, conversation=conversation, user=user
        )

        self.slots = []
        for checker in checkers:
            if checker.has_slots_to_return():
                custom_logger(checker, _type="string")
                self.slots = checker.slots
                break

        return self.slots

    def get_helpers(self, tracker, **kwargs) -> list:
        """
        Return a list of BaseHelper objects. They will be evaluated in sequence.
        """
        dispatcher = kwargs["dispatcher"]
        conversation = kwargs["conversation"]
        user = kwargs["user"]
        return [
            ExternalAuthenticationSlotsHelper(
                tracker=tracker,
                dispatcher=dispatcher,
                user=user,
                conversation=conversation,
                slots_type=SlotsType.LIST,
            ),
            NextCommentSlotsHelper(
                tracker=tracker,
                dispatcher=dispatcher,
                conversation=conversation,
            ),
        ]


class ValidateVoteForm(FormValidationAction):
    """
    This action is called when the vote form is active.
    After ActionAskVote ran, action_listen is activated and user should input.
    This action validates what user typed

    If the returned vote value is None, Rasa will call ActionAskVote again until a
    not-null value is returned by this action. This will assure that the chatbot keeps
    sending new comments to vote.

    https://rasa.com/docs/rasa/forms/#validating-form-input
    """

    def __init__(self, **kwargs):
        self.slots = []
        super().__init__(**kwargs)

    def name(self) -> Text:
        return "validate_vote_form"

    def validate_vote(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate vote value."""

        if not slot_value:
            return VoteDialogue.restart_vote_form_slots()

        if Vote.is_valid(slot_value):
            conversation = Conversation(tracker)
            try:
                response = self.create_vote(slot_value, tracker)
                if response.status_code == 201:
                    conversation.set_user_statistics()
            except EJCommunicationError:
                ej_client_error_manager = EJClientErrorManager()
                return ej_client_error_manager.get_slots(as_dict=True)

            checkers = self.get_helpers(
                tracker,
                dispatcher=dispatcher,
                slot_value=slot_value,
                conversation=conversation,
            )
            for checker in checkers:
                custom_logger(checker, _type="string")
                if checker.has_slots_to_return():
                    self.slots = checker.slots
                    break

            return {
                **self.slots,
                "access_token": tracker.get_slot("access_token"),
                "refresh_token": tracker.get_slot("refresh_token"),
            }

        else:
            dispatcher.utter_message(response="utter_invalid_vote_during_participation")
            return VoteDialogue.restart_vote_form_slots()

    def create_vote(self, slot_value, tracker: Tracker):
        vote = Vote(slot_value, tracker)
        comment_id = tracker.get_slot("current_comment_id")
        response = vote.create(comment_id)
        if response.status_code == 401:
            user = User(tracker)
            user.authenticate(force=True)
            vote = Vote(slot_value, tracker)
            response = vote.create(comment_id)
            if response.status_code == 401:
                raise EJCommunicationError
            tracker = user.tracker
        custom_logger(f"POST vote to EJ API: {vote}")
        return response

    def get_helpers(self, tracker, **kwargs):
        dispatcher = kwargs["dispatcher"]
        slot_value = kwargs["slot_value"]
        conversation = kwargs["conversation"]
        return [
            ExternalAuthenticationSlotsHelper(
                tracker=tracker,
                dispatcher=dispatcher,
                conversation=conversation,
                slots_type=SlotsType.DICT,
            ),
            UserCanAddCommentsSlotsHelper(
                tracker=tracker,
                dispatcher=dispatcher,
                conversation=conversation,
                slot_value=slot_value,
                slots_type=SlotsType.DICT,
            ),
            NeedToAskAboutProfileHelper(
                tracker=tracker,
                dispatcher=dispatcher,
                conversation=conversation,
                slots_type=SlotsType.DICT,
            ),
            RemainingCommentsSlotsHelper(
                conversation=conversation, dispatcher=dispatcher
            ),
        ]

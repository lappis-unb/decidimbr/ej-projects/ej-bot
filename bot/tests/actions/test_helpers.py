import pytest
from actions.helpers.vote_actions_helpers import (
    RemainingCommentsSlotsHelper,
    ExternalAuthenticationSlotsHelper,
    NextCommentSlotsHelper,
    UserCanAddCommentsSlotsHelper,
)
from actions.helpers.api_error_checker import EJClientErrorManager
from bot.ej.user import User
from ej.vote import SlotsType


class TestCheckRemainingCommentsSlots:
    def test_initialization(self, tracker, dispatcher, conversation):
        conversation.user_statistics["missing_votes"] = 0
        checker = RemainingCommentsSlotsHelper(
            dispatcher=dispatcher,
            conversation=conversation,
            slots_type="foo",
        )
        with pytest.raises(Exception):
            checker.has_slots_to_return()

        conversation.user_statistics["missing_votes"] = 1
        checker = RemainingCommentsSlotsHelper(
            dispatcher=dispatcher,
            conversation=conversation,
            slots_type=SlotsType.DICT,
        )

        assert checker.has_slots_to_return()
        assert type(checker.slots) == dict

        conversation.user_statistics["missing_votes"] = 0
        checker = RemainingCommentsSlotsHelper(
            dispatcher=dispatcher,
            conversation=conversation,
            slots_type=SlotsType.LIST,
        )

        checker.set_slots()
        assert checker.has_slots_to_return()
        assert type(checker.slots) == list

    def test_keep_vote_form_running(self, tracker, dispatcher, conversation):
        conversation.user_statistics["missing_votes"] = 2

        checker = RemainingCommentsSlotsHelper(
            dispatcher=dispatcher,
            conversation=conversation,
            slots_type=SlotsType.DICT,
        )

        assert checker.has_slots_to_return()
        assert checker.slots == {
            "vote": None,
            "user_statistics": conversation.user_statistics,
        }

    def test_completed_vote_form(self, tracker, dispatcher, conversation):
        conversation.user_statistics["missing_votes"] = 0

        checker = RemainingCommentsSlotsHelper(
            dispatcher=dispatcher,
            conversation=conversation,
            slots_type=SlotsType.DICT,
        )

        assert checker.has_slots_to_return()
        assert checker.slots == {
            "vote": "-",
            "conversation_has_comments_to_vote": False,
            "user_statistics": conversation.user_statistics,
        }


class TestCheckUserCanAddComentsSlots:
    def test_has_slots_to_return(self, tracker, dispatcher, conversation):
        tracker.set_slot("participant_can_add_comments", True)
        conversation.user_statistics["comments"] = 4

        checker = UserCanAddCommentsSlotsHelper(
            tracker=tracker,
            dispatcher=dispatcher,
            conversation=conversation,
            slot_value="1",
        )

        assert checker.has_slots_to_return()
        assert checker.slots == {
            "vote": "1",
            "ask_for_a_comment": True,
            "user_statistics": conversation.user_statistics,
        }

    def test_has_no_slots_to_return(self, tracker, dispatcher, conversation):
        tracker.set_slot("participant_can_add_comments", True)
        conversation.user_statistics["comments"] = 5

        checker = UserCanAddCommentsSlotsHelper(
            tracker=tracker,
            dispatcher=dispatcher,
            conversation=conversation,
            slot_value="1",
        )

        assert not checker.has_slots_to_return()
        assert checker.slots == []


class TestCheckNextCommentSlots:
    def test_has_slots_to_return(self, tracker, dispatcher, conversation, comment):
        checker = NextCommentSlotsHelper(
            tracker=tracker,
            dispatcher=dispatcher,
            conversation=conversation,
        )
        assert checker.has_slots_to_return()
        assert len(checker.slots) == 3
        assert checker.slots[1].get("value") == comment["content"]
        assert checker.slots[2].get("value") == comment["id"]


class TestCheckExternalAuthenticationSlots:
    def test_has_slots_to_return(self, tracker, dispatcher, conversation):
        conversation.anonymous_votes = 2
        conversation.user_statistics["comments"] = 2
        conversation.user_statistics["missing_votes"] = 2
        user = User(tracker)
        checker = ExternalAuthenticationSlotsHelper(
            user=user,
            tracker=tracker,
            dispatcher=dispatcher,
            conversation=conversation,
            slots_type=SlotsType.LIST,
        )
        assert checker.has_slots_to_return()
        print(checker.slots)
        assert checker.slots[0].get("value") == "-"
        assert checker.slots[1].get("value") == True


class TestEJApiErrorManager:
    def test_get_slots(self):
        ej_client_error_manager = EJClientErrorManager()
        slots = ej_client_error_manager.get_slots()
        assert slots[0].get("value") == "-"
        assert slots[1].get("value") == True

    def test_get_slots_as_dict(self):
        ej_client_error_manager = EJClientErrorManager()
        slots = ej_client_error_manager.get_slots(as_dict=True)
        assert slots.get("vote") == "-"
        assert slots.get("ej_client_connection_error") == True

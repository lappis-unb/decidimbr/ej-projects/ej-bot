import pytest
from unittest.mock import Mock, patch
from bot.ej.profile import (
    Profile,
    Question,
    Gender,
)


# Test initialization of the Profile object
def test_profile_initialization(profile):
    assert isinstance(profile.available_questions, list)
    assert isinstance(profile.remaining_questions, list)


# Test retrieving the user profile from the API
def test_get_user_profile(profile):
    assert profile.user == 1
    assert profile.gender == Gender.NOT_FILLED


# Test setting up the remaining questions based on the user profile
def test_set_remaining_questions(profile):
    assert len(profile.remaining_questions) > 0
    assert all(isinstance(q, Question) for q in profile.remaining_questions)


# Test validation of a valid answer
@patch("bot.ej.ej_client.requests.post")
def test_answer_is_valid_valid(mock_post):
    mock_post.return_value = Mock(ok=True)
    profile = Mock(spec=Profile)
    profile.available_questions = [
        Question(
            id=1,
            body="What is your gender?",
            answers=[{"payload": 1, "title": "Female"}],
            change=Gender,
            put_payload="gender",
        )
    ]
    profile.answer_is_valid.return_value = (True, None)

    is_valid, err = profile.answer_is_valid(1, 1)
    assert is_valid is True
    assert err is None


# Test validation of an invalid answer
def test_answer_is_valid_invalid(profile):
    profile.available_questions = [
        Question(
            id=1,
            body="What is your gender?",
            answers=[{"payload": 1, "title": "Female"}],
            change=Gender,
            put_payload="gender",
        )
    ]
    is_valid, err = profile.answer_is_valid(2, 1)
    assert is_valid is False
    assert err is None


# Test behavior when no more questions are available
def test_get_next_question_no_questions(profile):
    profile.remaining_questions = []
    with pytest.raises(Exception, match="No more questions to ask"):
        profile.get_next_question()


# Test retrieving the next question from the remaining questions
def test_get_next_question(profile):
    profile.remaining_questions = [
        Question(
            id=1,
            body="What is your gender?",
            answers=[{"payload": 1, "title": "Female"}],
            change=Gender,
            put_payload="gender",
        )
    ]
    message, question_id = profile.get_next_question()
    assert message["text"] == "What is your gender?"
    assert question_id == 1


# Test whether the system needs to ask about the user profile based on conversation statistics
def test_need_to_ask_about_profile(profile, conversation, empty_tracker):
    should_ask, next_count = profile.need_to_ask_about_profile(
        conversation, empty_tracker
    )
    assert should_ask is True
    assert next_count == 7

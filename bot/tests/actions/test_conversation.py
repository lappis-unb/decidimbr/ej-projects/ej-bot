from bot.ej.user import User
from ej.ej_client import EjClient
from ej.conversation import Conversation


class TestConversation:
    def test_user_should_not_authenticate_during_conversation(
        self, conversation, tracker
    ):
        conversation.anonymous_votes = 4
        user = User(tracker)
        assert not user.need_to_auth_in_bp(conversation)

    def test_user_should_authenticate_during_conversation(self, conversation, tracker):
        conversation.anonymous_votes = 5
        user = User(tracker)
        assert user.need_to_auth_in_bp(conversation)

    def test_init(self, conversation):
        assert conversation.id == "123"
        assert conversation.text == "Test Title"
        assert conversation.participant_can_add_comments
        assert conversation.anonymous_votes == 5
        assert isinstance(conversation.ej_client, EjClient)

    def test_create_conversation_without_api_data(self, tracker):
        conversation = Conversation(tracker)
        assert conversation.anonymous_votes == tracker.get_slot("anonymous_votes")
        assert conversation.participant_can_add_comments == tracker.get_slot(
            "participant_can_add_comments"
        )
        assert conversation.id == tracker.get_slot("conversation_id")
        assert conversation.text == tracker.get_slot("conversation_text")

    def test_create_conversation_with_empty_tracker(self, empty_tracker):
        data = {"anonymous_votes": 0, "participants_can_add_comments": False}
        empty_tracker.set_slot("participant_can_add_comments", True)
        conversation = Conversation(empty_tracker, data)
        assert conversation.anonymous_votes is not None
        assert conversation.anonymous_votes == 0
        assert not conversation.participant_can_add_comments

    def test_available_comments_to_vote(self, conversation):
        conversation.user_statistics["missing_votes"] = 1
        assert conversation.has_comments_to_vote()
        conversation.user_statistics["missing_votes"] = 0
        assert not conversation.has_comments_to_vote()

    def test_get_total_comments(self, conversation):
        assert conversation.get_total_comments() == 20

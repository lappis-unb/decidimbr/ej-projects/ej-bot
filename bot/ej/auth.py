from dataclasses import dataclass
from datetime import datetime, timezone, timedelta
from typing import Any, Dict, Text

import jwt

from ej.user import User

from .settings import (
    BP_EJ_COMPONENT_ID,
    EXTERNAL_AUTHENTICATION_HOST,
    JWT_SECRET,
    TOKEN_EXPIRATION_TIME,
)


class AuthenticationDialogue:
    END_PARTICIPATION_SLOT = "end_participant_conversation"
    CHECK_AUTHENTICATION_SLOT = "check_participant_authentication"

    @staticmethod
    def get_message():
        buttons = [
            {
                "title": "Confirmar",
                "payload": AuthenticationDialogue.CHECK_AUTHENTICATION_SLOT,
            },
            {
                "title": "Encerrar",
                "payload": AuthenticationDialogue.END_PARTICIPATION_SLOT,
            },
        ]
        return {
            "text": "Estou aguardando você se autenticar para continuar a votação. 😊",
            "buttons": buttons,
        }

    @staticmethod
    def restart_auth_form() -> Dict:
        return {"check_authentication": None, "has_completed_registration": None}

    @staticmethod
    def end_auth_form(has_completed_registration: bool) -> Dict:
        return {
            "check_authentication": AuthenticationDialogue.END_PARTICIPATION_SLOT,
            "has_completed_registration": has_completed_registration,
            "vote": None,
        }

    @staticmethod
    def completed_auth_form() -> Dict:
        return {
            "has_completed_registration": True,
            "vote": None,
        }

    @staticmethod
    def participant_refuses_to_auth(slot_value: Text):
        return slot_value == AuthenticationDialogue.END_PARTICIPATION_SLOT


@dataclass
class AuthenticationManager:
    """
    Controls the authentication URL creation using a JWT token to identify the chatbot user.
    """

    @classmethod
    def get_user_auth_url(cls, user: User, token_expiration: Any = None):
        """
        Returns an authentication URL with user token parameter.
        """
        token = cls.get_token(user, token_expiration)
        return cls.get_url(token)

    @classmethod
    def get_token(
        cls,
        user: User,
        token_expiration: Any = None,
    ) -> Text:
        """
        Returns a JWT string containing the user_id and secret_id fields.
        This token will be used by Brasil Particiativo platform to unify the
        the data (votes, comments and profile fields) provided by the same user across different channels.
        """
        if not JWT_SECRET:
            raise Exception("JWT_SECRET variable not found.")

        if not token_expiration:
            expiration_minutes = TOKEN_EXPIRATION_TIME.total_seconds()
            token_expiration = datetime.now(timezone.utc) + timedelta(
                minutes=expiration_minutes
            )

        data = {
            "user_id": user.sender_id,
            "secret_id": user.secret_id,
            "exp": token_expiration,
        }
        return jwt.encode(data, JWT_SECRET, algorithm="HS256")

    @classmethod
    def get_url(cls, token) -> Text:
        """
        Returns the authentication URL appended with the token argument.
        """
        if not EXTERNAL_AUTHENTICATION_HOST or not BP_EJ_COMPONENT_ID:
            raise Exception(
                "EXTERNAL_AUTHENTICATION_HOST or BP_EJ_COMPONENT_ID variables were not defined"
            )

        if not token:
            raise Exception("authentication token not is invalid")

        return f"{EXTERNAL_AUTHENTICATION_HOST}/{BP_EJ_COMPONENT_ID}/link_external_user?user_data={token}"

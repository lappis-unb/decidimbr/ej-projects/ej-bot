import base64
from dataclasses import dataclass
import hashlib
import json

from actions.logger import custom_logger
from ej.conversation import Conversation
from ej.ej_client import EjClient
from rasa_sdk import Tracker

from .routes import auth_route, registration_route
from .settings import ANONYMOUS_USER_NAME, SECRET_KEY
from enum import Enum


class InputChannelOptions(str, Enum):
    WHATSAPP = "whatsapp"
    TELEGRAM = "telegram"
    CMDLINE = "cmdline"
    UNKNOWN = "unknown"


class UserPasswordManager:
    """
    A class with utility methods to generate a password compatible with
    Ruby Base64 encode implementation.
    """

    @classmethod
    def buffer_to_base64(cls, buffer):
        """
         this is a hack to generate the same Decidim-encoded string for the user password.
         Every 60 characters, we need to insert a \n character in the Python base64-encoded string.
         Also, a \n character must be inserted at the end.

        https://ruby-doc.org/stdlib-2.5.3/libdoc/base64/rdoc/Base64.html
        """
        seed_base64 = base64.b64encode(buffer)
        ruby_compatible_base64 = ""
        for count, char in enumerate(list(seed_base64.decode())):
            ruby_compatible_base64 += char
            if not (count + 1) % 60:
                ruby_compatible_base64 += "\n"
        ruby_compatible_base64 += "\n"
        return ruby_compatible_base64

    @classmethod
    def to_sha256(cls, text):
        """
        Encrypt some text with SHA 256 algorithm.
        """
        return hashlib.sha256(text.encode()).hexdigest()


@dataclass
class User:
    tracker: Tracker = None
    has_completed_registration: bool = False
    phone_number: str = ""

    def __post_init__(self):
        if self.tracker:
            self.sender_id = self.tracker.sender_id
            self.phone_number = self._get_phone_number()
            self.email = self._get_email()
            self.name = self._get_name()
            self.display_name = self.name
            self.password = self._get_password()
            self.password_confirm = self.password
            self.has_completed_registration = self.tracker.get_slot(
                "has_completed_registration"
            )
            self.secret_id = UserPasswordManager.to_sha256(self.sender_id)
            self.ej_client = EjClient(self.tracker)

    def _get_name(self):
        metadata = self.tracker.latest_message.get("metadata")
        if metadata:
            if metadata.get("user_name"):
                return metadata.get("user_name")
            elif metadata.get("contact_name"):
                return metadata.get("contact_name")
        return ANONYMOUS_USER_NAME

    def _get_email(self):
        return f"{User.normalize_sender_id(self.sender_id)}-opinion-bot@mail.com"

    def _get_password(self):
        if SECRET_KEY and self.sender_id:
            seed = f"{self.sender_id}{SECRET_KEY}".encode()
            base64_hash = UserPasswordManager.buffer_to_base64(seed)
            return UserPasswordManager.to_sha256(base64_hash)
        raise Exception("could not generate user password")

    def _get_phone_number(self):
        input_channel = self.tracker.get_latest_input_channel()
        if input_channel == InputChannelOptions.WHATSAPP:
            return User.normalize_sender_id(self.sender_id)
        return ""

    def registration_data(self):
        data = {
            "name": self.name,
            "display_name": self.display_name,
            "password": self.password,
            "password_confirm": self.password_confirm,
            "email": self.email,
            "secret_id": self.secret_id,
            "has_completed_registration": False,
        }
        if self.phone_number:
            data["phone_number"] = self.phone_number

        return json.dumps(data)

    def auth_data(self):
        return json.dumps(
            {
                "name": self.name,
                "display_name": self.display_name,
                "password": self.password,
                "password_confirm": self.password_confirm,
                "email": self.email,
                "secret_id": self.secret_id,
            }
        )

    def need_to_auth_in_bp(self, conversation: Conversation) -> bool:
        """
        Verify if the user needs to be redirected to Brasil Participativo to authenticate.
        The Chatbot have two authentication methods: EJ API authentication and
        Brasil Participativo authentication. The first one creates a basic user in
        the EJ database. The second one updates the EJ user created by the chatbot
        with the GovBR email.

        The AuthenticationManager class controls how this redirection is made.
        """
        if not self.has_completed_registration:
            comments_counter = conversation.get_voted_comments()
            return comments_counter == conversation.anonymous_votes
        return False

    def authenticate(self, force=False):
        """
        Requests to EJ API an access_token and refresh_token.
        """
        access_token = self.tracker.get_slot("access_token")
        refresh_token = self.tracker.get_slot("refresh_token")
        custom_logger(f"TOKENS: {access_token} {refresh_token}")
        if (access_token and refresh_token) and not force:
            return self.tracker

        response = None
        try:
            custom_logger(
                f"Requesting new token for the participant", data=self.auth_data()
            )
            response = self.ej_client.request(auth_route(), self.auth_data())
            if response.status_code != 200:
                custom_logger(f"EJ API ERROR", data=response.json())
                raise Exception
        except Exception:
            custom_logger(
                f"Failed to request token, trying to create the participant",
                data=self.registration_data(),
            )
            response = self.ej_client.request(
                registration_route(), self.registration_data()
            )
            if response.status_code != 201:
                custom_logger(f"EJ API ERROR", data=response.json())
                raise Exception("COULD NOT CREATE USER")

        custom_logger(f"EJ API RESPONSE", data=response.json())
        response_data = response.json()
        self.tracker.slots["access_token"] = response_data["access_token"]
        self.tracker.slots["refresh_token"] = response_data["refresh_token"]
        self.has_completed_registration = response_data["has_completed_registration"]
        self.tracker.slots[
            "has_completed_registration"
        ] = self.has_completed_registration

    @classmethod
    def normalize_sender_id(cls, sender_id):
        for char in ":+":
            sender_id = sender_id.replace(char, "")
        return sender_id

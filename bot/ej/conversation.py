from dataclasses import dataclass
import logging

from ej.ej_client import EjClient
from rasa_sdk import Tracker

from .routes import conversation_route, random_comment_route, user_statistics_route
from .settings import EJCommunicationError


logger = logging.getLogger(__name__)


@dataclass
class Conversation:
    """Conversation controls requests to EJ API and some validations during bot execution."""

    def __init__(self, tracker: Tracker, data: dict = {}):
        self.tracker = tracker
        self.data = data
        self.id = self._get_id()
        self.text = self._get_text()
        self.participant_can_add_comments = self._get_participants_can_add_comments()
        self.anonymous_votes = self._get_anonymous_votes()
        self.ej_client = EjClient(self.tracker)
        self.send_profile_question = self._get_send_profile_question()
        self.votes_to_send_profile_questions = int(
            self._get_votes_to_send_profile_questions()
        )
        self.user_statistics = {}

    def _get_votes_to_send_profile_questions(self):
        if self.data and "votes_to_send_profile_question" in self.data.keys():
            return self.data.get("votes_to_send_profile_question")
        return (
            self.tracker.get_slot("votes_to_send_profile_questions")
            if self.tracker.get_slot("votes_to_send_profile_questions")
            else 0
        )

    def _get_send_profile_question(self):
        if self.data and "send_profile_question" in self.data.keys():
            return self.data.get("send_profile_question")
        return self.tracker.get_slot("send_profile_questions")

    def _get_id(self):
        if self.data and "id" in self.data.keys():
            return self.data.get("id")
        return self.tracker.get_slot("conversation_id")

    def _get_text(self):
        if self.data and "text" in self.data.keys():
            return self.data.get("text")
        return self.tracker.get_slot("conversation_text")

    def _get_anonymous_votes(self):
        if self.data and "anonymous_votes" in self.data.keys():
            return self.data.get("anonymous_votes")
        return self.tracker.get_slot("anonymous_votes")

    def _get_participants_can_add_comments(self):
        if self.data and "participants_can_add_comments" in self.data.keys():
            return self.data.get("participants_can_add_comments")
        return self.tracker.get_slot("participant_can_add_comments")

    @staticmethod
    def get(conversation_id: int, tracker: Tracker):
        ej_client = EjClient(tracker)
        try:
            response = ej_client.request(conversation_route(conversation_id))
            conversation = response.json()
            if response.status_code != 200:
                raise EJCommunicationError
            if len(conversation) == 0:
                raise EJCommunicationError
            if not conversation.get("id"):
                conversation["id"] = conversation_id
            return conversation
        except:
            raise EJCommunicationError

    def set_user_statistics(self):
        self.user_statistics = self.get_user_statistics()

    def get_user_statistics(self):
        try:
            url = user_statistics_route(self.id)
            response = self.ej_client.request(url)
            response = response.json()
        except:
            raise EJCommunicationError
        return response

    # TODO: refactor the try/except part to be a Python decorator.
    def get_next_comment(self):
        import time

        def _request_comment():
            response = self.ej_client.request(url)
            if response.status_code == 500:
                raise EJCommunicationError
            comment = response.json()
            if comment.get("content"):
                comment_url_as_list = comment["links"]["self"].split("/")
                comment["id"] = comment_url_as_list[len(comment_url_as_list) - 2]
                return comment
            return None

        url = random_comment_route(self.id)
        try:
            return _request_comment()
        except EJCommunicationError:
            time.sleep(2)
        try:
            return _request_comment()
        except Exception:
            raise EJCommunicationError

    def has_comments_to_vote(self) -> bool:
        if not self.user_statistics:
            self.user_statistics = self.get_user_statistics()
        return self.user_statistics["missing_votes"] >= 1

    def get_total_comments(self) -> int:
        if not self.user_statistics:
            self.user_statistics = self.get_user_statistics()
        return self.user_statistics["total_comments"]

    def get_voted_comments(self) -> int:
        if not self.user_statistics:
            self.user_statistics = self.get_user_statistics()
        return self.user_statistics["comments"]

    def user_can_add_comment(self, tracker: Tracker) -> bool:
        participant_can_add_comments = tracker.get_slot("participant_can_add_comments")
        if not participant_can_add_comments:
            return False
        total_comments = self.get_total_comments()
        voted_comments = self.get_voted_comments()
        return (total_comments >= 4 and voted_comments == 4) or (
            total_comments < 4 and voted_comments == 2
        )
